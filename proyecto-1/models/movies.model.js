const mongoose = require('mongoose');

const moviesSchema = new mongoose.Schema(

    {
        title:  { type: String, required: true },
        director:  { type: String, required: true },
        year:  { type: Number, required: true },
        genre:  { type: String, required: true },

    },
    
    {
        timestamps: true
    }

);

const moviesModel = mongoose.model('Movies', moviesSchema);

module.exports = moviesModel;