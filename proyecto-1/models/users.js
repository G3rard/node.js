const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({

    email: { 
        type: String, 
        required: true,
        unique: true,
        match: [/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/, 'introduce un email con formato valido']
    },
    password: { type: String, required: true}

},{

    timeatamp: true

});

const User = mongoose.model('user', userSchema);

module.exports = User;
