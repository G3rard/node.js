const mongoose = require('mongoose');

const cinemasSchema = new mongoose.Schema(

    {
        name: { type: String, required: true },
        location: { type: String, required: true },
        movies: [{ type: mongoose.Types.ObjectId, ref: 'Movies'}],

    },

    {
        timestamps: true
    }

);

const cinemasModel = mongoose.model('Cinemas', cinemasSchema);

module.exports = cinemasModel;