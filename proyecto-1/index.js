const express = require('express');
const moviesRouter = require('./routes/movies.routes.js');
const cinemaRouter = require('./routes/cinema.routes.js');
const userRouter = require('./routes/user.routes.js');
const connect = require('./utils/db/connect.js');
const MongoStore = require('connect-mongo');
const cors = require('cors');
const createError = require('./utils/errors/create-error.js');
const passport = require('passport');
const session = require('express-session');
const dataBase = "mongodb+srv://root:WwsEmhjtMxumx9rI@cluster0.zrgspwy.mongodb.net/?retryWrites=true&w=majority";

connect();


const PORT = 3000;
const server = express();

server.use(cors());

server.use(express.json());

server.use(express.urlencoded({ extended: false }));

require('./utils/authentication/passport.js');

server.use(session({
    secret: 'gr$5td#!2k421',
    resave: false,
    saveUninitialized: false,
    cookie: {
        maxAge: 360000
    },

    store: MongoStore.create({
        mongoUrl: dataBase
    })

}));

server.use(passport.initialize());

server.use(passport.session());


server.use('/user', userRouter)
server.use('/', moviesRouter);
server.use('/cinemas', cinemaRouter);
server.use('*', (req, res, next) => {
    const error = createError('Esta ruta no existe', 404);
    next(error);
});


server.use((err, req, res, next) => {
    return res.status(err.status || 500).json(err.message || 'Unexpected error');
});



server.listen(PORT, () => {
    console.log(`server is online in http://localhost:${PORT}`);
});