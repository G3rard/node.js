const mongoose = require('mongoose');
const moviesModel = require('../../models/movies.model.js');
const fs = require('fs');

const dataBase = "mongodb+srv://root:WwsEmhjtMxumx9rI@cluster0.zrgspwy.mongodb.net/?retryWrites=true&w=majority";

mongoose.connect(dataBase, {

    useNewUrlParser: true,
    useUnifiedTopology: true,

}).then(async () => {

    const allMovies = await moviesModel.find();

    if (allMovies.length) {

        await moviesModel.collection.drop();

    }
}).catch(err => {
    console.log(`Ha habido un error eliminando los datos: ${err}`);
})
.then(async () => {

    const data = fs.readFileSync('./utils/seeds/db/movies.json');

    const parseData = JSON.parse(data);
    const moviesDocs = parseData.map((movies) => {
        return new moviesModel(movies)

    });
    await moviesModel.insertMany(moviesDocs);
})
.catch((err) => {
    console.log(`Ha habido un error ${err} `);
})
.finally(() => mongoose.disconnect);