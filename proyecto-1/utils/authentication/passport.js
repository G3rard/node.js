const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const User = require('../../models/users');
const bcrypt = require('bcrypt');
const createError = require('../errors/create-error.js')

passport.use(
    'register',
    new LocalStrategy({
        usernameField: "email",
        passwordField: "password",
        passReqToCallback: true
    },
        async (req, email, password, done) => {

            try {

                const checkUser = await User.findOne({ email });

                if (checkUser) {

                    return done(createError('el usuario ya esta registrado'));
                }

                const cryPassword = await bcrypt.hash(password, 10);

                const newUser = new User({
                    email,
                    password: cryPassword
                });

                const saveUser = await newUser.save();

                return done(null, saveUser)
            } catch (err) {

                return done(err);

            }

        }

    )
);


passport.use(
    'login',
    new LocalStrategy(
        {
            usernameField: 'email',
            passwordField: 'password',
            passReqToCallback: true
        },

        async (req, email, password, done) => {

            try{
            
            const checkUser = await User.findOne({ email });

            if(!checkUser) {
                return done(createError('Este usuario no existe en la base de datos.'))
            }

            const passwordCheck = await bcrypt.compare(
                password,
                checkUser.password
                );

                if(!passwordCheck){
                    return done(createError('la contraseña es invalida'))
                }

                checkUser.password = null;
                return done(null, checkUser);

            }catch(err){

               return done(err);
            }

        }
    )

);
  


passport.serializeUser((user, done) => {
    
    return done(null, user._id);
    

});


passport.deserializeUser(async (userId, done) => {
    try {
        const existingUser = await User.findById(userId);
        return done(null, existingUser);
    } catch (err) {
        return done(err);
    }
});