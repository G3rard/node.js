const express = require('express');
const Movies = require('../models/movies.model.js');
const createError = require('../utils/errors/create-error.js')
const isAuthPassport = require('../utils/middleware/auth-passport.middleware.js');

const moviesRouter = express.Router();

moviesRouter.get('/', async (req, res, next) => {
    try {

        return res.status(200).json('Bienvenido a la base de datos sobre peliculas');

    } catch (err) {

        next(err);
    }


});


// --------------endpoint de todas las peliculas---------------------

moviesRouter.get('/all', async (req, res, next) => {
    try {
        const movies = await Movies.find();
        return res.status(200).json(movies);

    } catch (err) {

        next(err);
    }


});


// -----------------------------endpoint filtrado por año-------------------------------------

moviesRouter.get('/movies/year', async (req, res, next) => {
    try {
        const movies = await Movies.find(

            { year: { $gte: 2010 } }

        );
        return res.status(200).json(movies);

    } catch (err) {

        next(err);
    }


});

// ------------------------endpoint filtrado por titulos-----------------------------------

moviesRouter.get('/movies/title/:title', async (req, res, next) => {
    const title = req.params.title;

    try {

        const movies = await Movies.find(
            { title: { $in: [title] } }
        );

        if (movies == '') {

            next(createError('La pelicula no existe o esta mal escrita', 404));


        } else {
            return res.status(200).json(movies);

        }


    } catch (err) {

        next(err);
    };


});




// --------------------------endpoint filtrado por genero-----------------------------

moviesRouter.get('/movies/genre/:genre', async (req, res, next) => {
    const genre = req.params.genre;
    try {
        const movies = await Movies.find(
            { genre: { $in: [genre] } }
        );

        if (movies == "") {

            next(createError('La categoria no existe', 404));

        } else {

            return res.status(200).json(movies);

        }

    } catch (err) {

        next(err)

    };


});

// -----------------------endpoint filtrado por id----------------------------------------------

moviesRouter.get('/movies/:id', async (req, res, next) => {
    const id = req.params.id;
    try {
        const movies = await Movies.findById(id);
        
        if (movies) {
           
            return res.status(200).json(movies);

        } else {

            next(createError("el ID de la pelicula no existe", 404));
        }

    } catch (err) {

        next(err)
    }


});

// ---------POST para agregar una nueva pelicula-----------------------

moviesRouter.post('/all', [isAuthPassport], async (req, res, next) => {

    try {

        const newMovie = new Movies({ ...req.body })

        const createMovies = await newMovie.save();

        return res.status(201).json(createMovies);


    } catch (err) {

        next(err);
    }
});

// ---------------Delete para eliminar una pelicula-------------------------

moviesRouter.delete('/movies/:id', [isAuthPassport], async (req, res, next) => {
    try {
        const id = req.params.id;
        const deleteMovie = await Movies.findByIdAndDelete(id);
        return res.status(200).json('El elemento ha sido eliminado correctamente');

    } catch (err) {

        next(err);
    }
});

// ---------------------------Put para actualizar los valores de una pelicula en especifico----------------------------------------

moviesRouter.put('/movies/:id', [isAuthPassport], async (req, res, next) => {

    try {
        const id = req.params.id;
        const modifiedMovie = new Movies({ ...req.body })

        modifiedMovie._id = id;

        const movieUpdt = await Movies.findByIdAndUpdate(

            id,
            { $set: { ...modifiedMovie } },
            { new: true }

        );

        return res.status(200).json('El elemento ha sido actualizado correctamente');




    } catch (err) {

        next(err)
    }

});


module.exports = moviesRouter;