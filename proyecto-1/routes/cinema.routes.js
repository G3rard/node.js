const express = require('express');
const Cinemas = require('../models/cinemas.models.js');
const createError = require('../utils/errors/create-error.js');
const isAuthPassport = require('../utils/middleware/auth-passport.middleware.js');
// ----------------- ruta de todos los cines--------------------
const cinemaRouter = express.Router();

cinemaRouter.get('/', async (req, res, next) => {
    try {

        const cinemas = await Cinemas.find().populate('movies');
        return res.status(200).json(cinemas);
        
    } catch (err) {
        next(err);
    }
})


// ----------------------POST para agregar cines-------------------------

cinemaRouter.post('/', [isAuthPassport], async (req, res, next) => {

    try {

        const newCinema = new Cinemas({ ...req.body })

        const createCin = await newCinema.save();

        return res.status(201).json(createCin);


    } catch (err) {

        next(err);
    }
});

// -----------put para agregar peliculas en un cine especifico--------------

cinemaRouter.put('/addmovies', [isAuthPassport], async (req, res, next) => {

    try {
        const { movieId, locationId } = req.body;
        if(!locationId) {
           return next(createError('Se necesita un id de la localizacion para añadir las peliculas', 500));
        }if(!movieId){
            return next(createError('Se necesita la id de la pelicula para agregarla'));
        
        }else{
            const updateMovies = await Cinemas.findByIdAndUpdate(
                locationId,
                { $push: { movies: movieId }},
                { new: true }
            )
            return res.status(200).json('Las peliculas han sido actualizadas');
        };
        
    } catch (err) {

        next(err);

    }

})

// --------------Delete para borrar cines-----------------------------

cinemaRouter.delete('/:id', [isAuthPassport], async (req, res, next) => {
    try{
        const id = req.params.id;
        const deleteCinema = await Cinemas.findByIdAndDelete(id);
        return res.status(200).json('El elemento ha sido eliminado correctamente');

    }catch(err){
       
        next(err);
    }
});


module.exports = cinemaRouter;