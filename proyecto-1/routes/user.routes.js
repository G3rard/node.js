const passport = require('passport');
const express = require('express');

const userRouter = express.Router();


// ------------------------ruta del registro---------------------

userRouter.post('/register', (req, res, next) => {

    const done = (err, user) => {
        if (err) {
            return next(err);
        }

        req.logIn(

            user,
            (err) => {
                if (err) {
                    return next(err);
                }
                return res.status(201).json('Te has registrado correctamente');
            }


        );


    };

   passport.authenticate('register', done)(req);


});
// ----------------------------ruta del login---------------------------


userRouter.post('/login', (req, res, next) => {

    const done = (err, user) => {

        if (err) {
            return next(err);
        }

        if (req.user){
            return res.status(200).json('La sesion ya esta activa');
        }

        req.logIn(

            user,
            (err) => {
                if (err) {
                    return next(err);
                }
                return res.status(200).json('Has iniciado sesion correctamente');
            }


        );


    };

   passport.authenticate('login', done)(req);


});


// ---------------------------ruta para cerrar sesion-------------------------

userRouter.post('/logout', (req, res, next) => {

    if(req.user) {
        req.logOut(() => {    req.session.destroy(() => {
            res.clearCookie('connect.sid');
            return res.status(200).json('Has cerrado sesion correctamente');
        })
    });

    

    }else {
        return res.status(304).json('No hay ninguna sesion activa')
    }

});




module.exports = userRouter;